# Install Kubernets with Kubeadm
## Before you begin
* One or more machines running of:
    * CentOS 7.x
    * RedHat Enterprise Linux Server (RHEL) 7.x
* RAM: Minimum 2GB per machine
* CPU: Minimum 2 CPUs
* Full network connectivity between all machines in the cluster
* Unique hostname, MAC address, and product_uuid for every node
* Certain ports are open on your machines
* Swap disabled. You MUST disable swap in order for the kubelet to work properly.

## Verify the MAC address and product_uuid are unique for every node
* Check MAC address with command:
```
ip link
ifconfig -a
```

* Check ``product_uuid`` by:
```
cat /sys/class/dmi/id/product_uuid
```
## Configure Host file
```
cat <<'EOF'>> /etc/hosts
192.168.43.51   node01  node01.lab.local
192.168.43.52   node02  node02.lab.local
192.168.43.53   k8smas01    k8smas01.lab.local
EOF
```
* Permit ``SELinux``
```
sed -i 's/SELINUX=.*/SELINUX=permissive/' /etc/selinux/config
setenforce 0
```

* Disable ``swap``
```
swapoff -a
sed -i 's/^.*swap/#&/' /etc/fstab
```

* Enable ``br_netfilter``
```
cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system
modprobe br_netfilter
```

## Check required ports
* Control-plane node(s)
```
firewall-cmd --permanent --new-service=SVR_K8S_CPN
for n in 6443 2379 2380 10250 10251 10252; 
do
    echo "Add Control-plane port $n into SVR_K8S_CPN....."
    firewall-cmd --permanent --service=SVR_K8S_CPN --add-port=$n/tcp
done
firewall-cmd --permanent --add-service=SVR_K8S_CPN
firewall-cmd --reload
```

* Worker node(s)
```
firewall-cmd --permanent --new-service=SVR_K8S_WON
for m in 10250 30000-32767;
do
    echo "Add Worker node port $m into SVR_K8S_WON....."
    firewall-cmd --permanent --service=SVR_K8S_WON --add-port=$m/tcp
done
firewall-cmd --permanent --add-service=SVR_K8S_WON
firewall-cmd --reload
```

## Install Docker as runtime
* Here is the reference [Docker Installation](https://docs.docker.com/install/linux/docker-ce/centos/)
* Uninstall old Docker versions
```
yum -y remove docker \
    docker-client \
    docker-client-latest \
    docker-common \
    docker-latest \
    docker-latest-logrotate \
    docker-logrotate \
    docker-engine
```
* Installing required packages. ``yum-utils`` provides the yum-config-manager utility, and ``device-mapper-persistent-data`` and ``lvm2`` are required by the devicemapper storage driver.
```
yum -y install yum-utils \
	device-mapper-persistent-data \
	lvm2
```

* Installing Docker Engine - Community stable repository
```
yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo
```
* Install ``container-selinux`` as for requirement. In case RHEL not subscription, so we need to install that package from CentOS mirror instead. Check [Lat container-selinux](http://mirror.centos.org/centos/7/extras/x86_64/Packages/)
```
yum -y install http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.107-3.el7.noarch.rpm
```

* Installing Docker Engine packages
```
yum -y install docker-ce docker-ce-cli containerd.io
systemctl enable --now docker
```

## Installing Kubeadm, kubelet and kubectl
* Create Kubernets repo
```
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
```

* Installing Kubernets packages
```
yum -y install kubelet kubeadm kubectl --disableexcludes=kubernetes
systemctl enable --now kubelet
```

## Configure cgroup driver used by kubelet on control-plane node


## Deploying the Dashboard (UI)
* Reference [Deploying Dashboard (UI)](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/)